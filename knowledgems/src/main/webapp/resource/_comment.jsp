<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<br><br>
<div class="re-box" id="comment-box">
    <div class="resource-comment" id="comment-title">
        <h6 class="font-2">评论</h6>
        <c:if test="${commentList.size() == 0}">
            <div class="font-text-darkgray">
                此资源暂无评论~
            </div>
        </c:if>
    </div>
    <c:if test="${commentList.size() != 0}">
        <c:forEach var="com" items="${commentList}">
            <div class="resource-comment" id="${com.id}">
                <div class="col-lg-12">
                        <%---------------------- 评论头像 ----------------------%>
                    <div class="resource-comment-img">
                        <img src="/assets/images/profilePic/${com.picName}">
                    </div>
                        <%---------------------- 评论内容 ----------------------%>
                    <div class="resource-comment-cont">
                        <div class="resource-comment-head row">
                                <%---------------------- 评论用户 ----------------------%>
                            <a href="/myresource?userId=${com.userId}" class="resource-comment-name">${com.userName}</a>
                                <%---------------------- 评论操作 ----------------------%>
                            <c:if test="${com.userId == userId}">
                                <a type="button" class="resource-comment-name pull-right font-text-darkgray"
                                   style="text-decoration: underline" onclick="deleteComment(${com.id})">
                                    删除评论
                                </a>
                            </c:if>
                        </div>
                            <%-- 正常显示 --%>
                        <div class="resource-comment-text" id="comment-content-${com.id}">
                                ${com.content}
                        </div>
                            <%-- 折叠显示 --%>
                        <div class="resource-comment-text resource-comment-gradient" style="display: none;" id="comment-min-${com.id}">
                        </div>
                            <%-- 折叠按钮 --%>
                        <div class="resource-comment-date row" id="comment-bottomOff-${com.id}">
                            <fmt:formatDate value="${com.time}" pattern="yyyy-MM-dd HH:mm:ss"/>
                            <a class="resource-comment-name pull-right" id="comment-foldOn-${com.id}"
                               href="javascript:doFold(0, ${com.id})" style="display: none">收起</a>
                            <a class="resource-comment-name pull-right" id="comment-foldOff-${com.id}"
                               href="javascript:doFold(1, ${com.id})" style="display: none">展开</a>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:if>
    <%---------------- 分页 ----------------%>
    <br>
    <div id="page-data" class="text-center">
        <div id="page-total" style="display: none">${pageTotal}</div>
    </div>
    <%---------------- 编辑器 ----------------%>
    <br>
    <div class="row">
        <div class="col-12">
            <div id="editor" autofocus></div>
        </div>
    </div>
    <button id="submitCommentButton" class="re-btn re-label-child pull-right" onclick="submitRichComment();">发表评论</button>
</div>

<%-- tinymce --%>
<script src="../assets/vendor/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
    tinymce.init({
        selector: '#editor',
        contextmenu: "bold copy | link | image | imagetools | table | spellchecker",//上下文菜单
        auto_focus: false,
        menubar: false,
        language: 'zh_CN',
        // force_br_newlines: true,
        // force_p_newlines: false,
        // forced_root_block: "",
        // preformatted: true,
        toolbar_mode: 'sliding',
        branding: false,
        content_style: "img {height:auto;max-width:100%;max-height:100%;}",
        plugins: [
            'powerpaste', // plugins中，用powerpaste替换原来的paste
            'image',
            "autoresize",
            "code",
            "link",
            "charmap",
            "emoticons",
            "table",
            "wordcount",
            "advlist",
            "lists",
            "indent2em",
            "hr",
            "toolbarsticky",
            "textpattern"
            //...
        ],
        min_height: 300, //编辑区域的最小高度
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt",
        font_formats: "微软雅黑='微软雅黑';宋体='宋体';黑体='黑体';仿宋='仿宋';楷体='楷体';隶书='隶书';幼圆='幼圆';Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings",
        powerpaste_word_import: 'propmt',// 参数可以是propmt, merge, clear，效果自行切换对比
        powerpaste_html_import: 'propmt',// propmt, merge, clear
        powerpaste_allow_local_images: true,
        paste_data_images: true,
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open("POST", "${pageContext.request.contextPath}/util/uploadImage");
            formData = new FormData();
            formData.append("file", blobInfo.blob(), blobInfo.filename());
            xhr.onload = function (e) {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(this.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            xhr.send(formData);
        },
        toolbar: [
            'code insertfile undo redo emoticons | formatselect | forecolor backcolor charmap bold italic underline strikethrough hr | fontselect | fontsizeselect | blockquote subscript superscript removeformat |',
            'bullist numlist | alignleft aligncenter alignright alignjustify outdent indent indent2em | link image table'
        ],
        init_instance_callback: function () {
            loading("reset");
            // var htmlStr = "";
            // $("#loading_editor").html(htmlStr);
        },
        // 工具栏吸附
        my_toolbar_sticky: true,
        toolbar_sticky_always: true,
        toolbar_sticky_elem: "nav",
        toolbar_sticky_elem_autohide: false,
        // placeholder
        placeholder: "留下你对本资源的评论吧~（请不要超过800字）"
    });

    // 评论区折叠高度
    let limitHeight = 200;

    $(document).ready(function () {
        // 评论区折叠
        $("#comment-box > div").each(function(){
            let id = $(this).attr('id');
            if (id != 'comment-title'){
                // 若超出限制高度则需要折叠
                if ($("#comment-content-" + id).outerHeight() > limitHeight) {
                    let content = $('#comment-content-' + id).html();

                    /** ============== 3.0版本：直接限制高度截断，增加css特效样式 ============== */
                    //填充缩略区域
                    $('#comment-min-' + id).html(content);
                    $('#comment-min-' + id).css('height', limitHeight.toString() + 'px');

                    /** ============== 2.0版本：不替换<p标签 ============== */
                    // let contentTemp = content.replace(/<(style|script|iframe)[^>]*?>[\s\S]+?<\/\1\s*>/gi,'').replace(/(<[^p\/][^>]+?>|<\/[^p][^>]+?>)/g,'');
                    // let contentMin = "";
                    // let cutFlag = 0;
                    // let heightFlag = false;
                    // let availableLength = 250;
                    // while (cutFlag === 0) {
                    //     let pStart = contentTemp.indexOf("<p");
                    //     if (pStart === -1) {
                    //         contentMin += contentTemp.substring(0, availableLength);
                    //         if (availableLength > contentTemp.length) {
                    //             cutFlag = 1; //1.完全显示
                    //         } else {
                    //             cutFlag = 2; //2.在末尾发生截断
                    //         }
                    //     } else {
                    //         let pStartEnd = contentTemp.indexOf(">");
                    //         let pEnd = contentTemp.indexOf("</p>");
                    //         let pLength = (pEnd - pStartEnd + pStart);
                    //         if (pLength > availableLength || pEnd === -1) {
                    //             cutFlag = 3; //3.在段中发生截断
                    //         } else {
                    //             availableLength -= pLength;
                    //             contentMin += contentTemp.substring(0, pEnd + 4);
                    //             contentTemp = contentTemp.substring(pEnd + 4);
                    //         }
                    //     }
                    // }
                    // contentMin += " ...";
                    // //填充缩略区域
                    // $('#comment-min-' + id).html(contentMin);
                    // if ($('#comment-min-' + id).outerHeight() < $(this).outerHeight()) {
                    //     heightFlag = true;
                    // }

                    /** ============== 1.0版本：直接替换全部标签 ============== */
                    // let contentMin = content.replace(/<(style|script|iframe)[^>]*?>[\s\S]+?<\/\1\s*>/gi,'').replace(/<[^>]+?>/g,'')
                    //     .replace(/\s+/g,' ').replace(/ /g,' ').replace(/>/g,' ').substring(0, 400) + '...';
                    // //填充缩略区域
                    // $('#comment-min-' + id).html(contentMin);

                    // 显示缩略区域
                    $('#comment-content-' + id).css('display','none');
                    $('#comment-min-' + id).css('display','block');
                    // 显示折叠按钮
                    $('#comment-foldOn-' + id).css('display','none');
                    $('#comment-foldOff-' + id).css('display','block');
                }
            }
        });
    })
</script>