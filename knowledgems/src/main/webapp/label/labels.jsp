<%--
  Created by IntelliJ IDEA.
  User: liyunze
  Date: 2020/9/20
  Time: 下午12:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="zh-Hans">
<head>
    <c:import url="../template/_header.jsp"/>
    <link rel="stylesheet" type="text/css">
    <script src="../assets/js/labels.js"></script>

    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/newCSS.css">

    <title>分类-知了[团队知识管理应用]</title>
</head>
<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=label"/>
    <div class="re-box-6 mt-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="re-box re-box-decorated">
                        <div class="re-box-content">
                            <div id="labels" class="re-form-group"></div>
                        </div>
                    </div>
                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="re-box re-box-decorated">
                        <div id="search-data" class="resource-post">
                            <div class="resource-post-box">
                                <div class="re-loading">
                                    <img src="../assets/images/loading.gif"/>
                                </div>
                                <div class="panel-body text-center">正在加载请稍候</div>
                            </div>
                        </div>
                        <div id="page-data" class="text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <c:import url="../template/_footer.jsp"/>
</div>
</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script>
    /* 标签数据 */
    var labelList = [];
    var labelIds = "";
    var  test = '${label_id}'

    /* 加载动画 */
    var loadinghtml = "<div class=\"resource-post-box\">" +
        "<div class=\"re-loading\">" +
        "<img src=\"../assets/images/loading.gif\"/>" +
        "</div>" +
        "<div class=\"text-center\">正在加载请稍候</div>" +
        "</div>"

    /* 获取标签 */
    mount();
    function mount() {
        $.ajax({
            type: "get",
            url: "/labels",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    labelList = data.result;
                    load(labelList);
                    if(test.length == 0){
                        getPage(1);
                    }else {
                        labelButtonLoad(test);
                    }
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }

    /*加载单个标签*/
    function labelButtonLoad(label_id){
        labelButton(label_id,23,label_id)
    }

    /* 载入标签 */
    function load(tagList) {
        var htmlStr = "";
        for (var j = 0; j < tagList.length; j++) {
            htmlStr += "<div class=\"col-md-12 row\">";
            for (var i = 0; i < tagList[j].length; i++) {
                switch (i) {
                    case 0: {
                        htmlStr += "<div>";
                        htmlStr += "<a class=\"re-btn re-label-header\" disabled=\"disabled\">" + tagList[j][i].name + "</a>";
                        htmlStr += "</div>";
                        htmlStr += "<div class=\"col-md-12\">";
                        break;
                    }
                    default: {
                        var tagid = tagList[j][i].id;
                        var tempGroup = "group" + tagid;
                        var uplevelid = tagList[j][i].uplevelid;
                        htmlStr += "<div class='float-left'>";
                        htmlStr += "<button id=\"" + tempGroup + "\" class=\"re-btn re-btn-unselected re-label-child\" onclick=\"labelButton(" + tempGroup + "," + uplevelid + "," + tagid + ")\">" + tagList[j][i].name + "</button>";
                        htmlStr += "</div>";
                    }
                }
            }
            htmlStr += "</div>";
            htmlStr += " <br/>";
            htmlStr += "</div>";
        }
        $("#labels").html(htmlStr);
    }

    /* 标签按钮 */
    function labelButton(tempGroup,uplevelid,tagid){
        var tempGroup = "group" + tagid;
        if($("#" + tempGroup).hasClass("re-btn-unselected")){
            $("#" + tempGroup).removeClass("re-btn-unselected");
            labelIds = labelIds + ',' + tagid;
        } else {
            $("#" + tempGroup).addClass("re-btn-unselected");
            labelIds = labelIds.replace(',' + tagid, '');
        }
        console.log("selected-labelIds: " + labelIds);

        var loading = "<div class=\"resource-post-box\">\n" +
            "<div class=\"front-loading\">\n" +
            "<img src=\"../assets/images/loading.gif\"/>" +
            "</div>\n" +
            "<div class=\"panel-body text-center\">正在加载请稍候</div>\n" +
            "</div>"
        $("#search-data").html(loading);
        var pagesize = "";
        $("#page-data").html(pagesize);
        getPage(1);
    }

    /* 获取页码 */
    function getPage(page) {
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../search/searchdata",{
            queryStr:"",
            page: page-1,
            groupIds:"",
            labelIds:labelIds.substring(1),
            searchTimes:1,
        },function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString(""+data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("appNum").value);
            $("#search-data").html(data);
            //页数大于一，分页
            if(pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page,pageSize,"getPage")+"<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }
</script>
<!-- END: Scripts -->
</html>