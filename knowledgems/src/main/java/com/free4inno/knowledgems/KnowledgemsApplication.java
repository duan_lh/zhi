package com.free4inno.knowledgems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * KnowledgemsApplication.
 */
@SpringBootApplication
public class KnowledgemsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(KnowledgemsApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(this.getClass());
    }
}
