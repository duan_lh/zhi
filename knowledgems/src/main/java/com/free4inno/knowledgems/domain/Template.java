package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/4/27.
 */

@Entity
@Table(name = "template")
@Data
public class Template {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;    // id（主键）

    @Column(name = "templateName", columnDefinition = "TEXT")
    private String templateName; //预留字段

    @Column(name = "templateCode", columnDefinition = "LONGTEXT")
    private String templateCode; //预留字段

    @Column(name = "createTime")
    private Timestamp createTime; //预留字段

    @Column(name = "setTime")
    private Timestamp setTime; //预留字段

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getSetTime() {
        return setTime;
    }

    public void setSetTime(Timestamp setTime) {
        this.setTime = setTime;
    }

}
